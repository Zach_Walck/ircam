# Accueil

Bienvenue sur le site du groupe x du [projet BA2 2023-2024](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2022-2023/enonce/#code-de-depart) de la [filière biomédicale](https://polytech.ulb.be/fr/les-etudes/masters/ingenieur-civil-biomedical) de l'[Université Libre de Bruxelles](https://www.ulb.be)

Par le biais de ce site, nous présentons notre projet d'année qui est la conception d'une caméra thermique infrarouge à l'aide d'un capteur de température sans contacts.


## Présentation du groupe (et vidéo)

Le groupe est composé de x étudiants dont voici une belle photo : 

![Groupe x](img/group.jpg)

[source de l'image](https://www.cewe.be/fr/blog/2015/11/03/5-idees-pour-des-photos-de-groupe-originales-2/)

Alternative, intégration d'une vidéo de présentation du groupe sur youtube ;-)

![type:video](https://www.youtube.com/embed/_DZyLQFeoTY)

Attention, **pour intégrer une vidéo youtube**, vous devez mettre comme _url_ la concaténation de _https://www.youtube.com/embed/_ suivi de l'identificateur de la vidéo qui peut être trouvé, par exemple, après l'égal du _watch?v=_ du lien de visionage youtube.

Pour l'intégration de vidéos, vous pouvez aller lire la [documentation suivante](https://pypi.org/project/mkdocs-video/).

**Dans tous les cas, évitez absolument d'ajouter une vidéo enregistrée au git et utilisez les plateformes en ligne pour les diffuser !**


## Plan du site

1. [MkDocs](mkdocs.md) présente la documentation originale fournie avec le template _mkdocs_ **(à supprimer)**.
2. [Enoncé du projet](enonce.md) reprend l'énoncé comme exemple complémentaire à l'utilisation de _mkdocs_ **(à supprimer éventuellement)**.
3. [Précision du capteur](precision_capteur_temp.md) montre les étapes expérimentales qui ont permis l'évaluation de la précision du capteur de température sans contacts qui a été choisi.
4. [Caméra thermique](dispositif.md) présente le dispositif construit permettant l'acquisition d'images thermiques à l'aide du capteur thermique monopoint.
5. [Traitement des données](processing.ipynb) montre les différentes étapes de traitement de données dans un jupyter notebook python.
6. [Images thermiques](images.md) montre plusieurs images thermiques obtenues avec le dispositif construit.
