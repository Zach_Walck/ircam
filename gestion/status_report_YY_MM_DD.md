# Status report

- Groupe : x
- Date : DD/MM/YYYY

## Ordre du jour

- [Plan d'ensemble](#plan-d'ensemble)
- [Activités](#activités)
- [Risques et problèmes](#risques-et-problèmes)
- [Discussion de contenu](#discussions-de-contenu)

## Plan d'ensemble

![Plan](plans/yy_mm_dd.jpg)

## Activités 

| Activités terminées la semaine passée | Activités planifiées la semaine prochaine | 
| ------ | ------ |
| ...(PS-8/10) | Activité planifiée 1 |
| ... | Activité planifiée 2 |
|  | Activité plantifiée 3 |
|  | Activité plantifiée 4 |

## Risques et problèmes

Sous forme de **tableaux** mais en _Markdown_ pur, **il n'est pas possible d'afficher plusieurs lignes par cellule**.

Utilisation d'une seule ligne par cellule et de la balise html `<br>` pour passer à la ligne


|                        | Medium Probability  | High Probability | Issue  |
| ------                 | ------              | ------           | ------ |
| **Medium Impact**      | - Risque 7<br>- Risque 8 |- Risque 1<br>- Risque 2| Problème 1 |
| **High Impact**               | - Risque 3<br>- Risque 4 | - Risque 5<br>- Risque 6 | :warning: Problème 2 |

## Discussions de contenu

- Transfert de connaissance
- Débat
- Décisions / choix à prendre
- Gestion de conflit

