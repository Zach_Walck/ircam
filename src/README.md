# Logiciel de contrôle, traitement et affichage des images thermiques

Mettez dans ce répertoire le logiciel (python) du dispositif utilisant une communication série du PC via l'USB de programmation du microcontrôleur.

Ce logiciel s'occupera également d'enregistrer les données brutes, de les traiter et d'afficher les images thermiques. 

Notons qu'il peut être constitué de plusieurs fichiers _python_ séparés. Chaque fichier peut éventuellement s'occuper d'une tâche définie. 

Dans les sous-dossier [exemples](./exemples), vous trouverez plusieurs exemples de scripts python permettant différentes tâches dont l'acquisition de données. Veuillez lire le [README.md](./exemples/README.md) de ce dossier pour plus d'informations à ce sujet !
