#!/usr/bin/env python
import sys
import time
import serial
from argparse import ArgumentParser

#arguments parser
argparse = ArgumentParser()
argparse.add_argument('-p', '--port', type=str, help='Microcontroler serial port (COMx under windows, /dev/ttyUSBx under linux)', default="COM3")
argparse.add_argument('-f','--file', type=str, help='File output', default="samples.txt")
argparse.add_argument('-t','--timeout', type=int, help='Time to wait for data on the serial before aborting in seconds. Please choose a timeout above the inverse of the sampling frequency !', default=10)
argparse.add_argument('-b','--baudrate', type=int, help='Serial port baud rate', default=115200)
argparse.add_argument('-F','--frequency', type=float, help='Sampling frequency for data', default=20)
argparse.add_argument('-l','--length', type=float, help='Samples file length in seconds (default 10min)', default=600)
argparse.add_argument('-m','--method', type=int, help='Method number, 0 for Grab, 1 for Samples, 2 for asking data with fixed time interval and 3 with accumulated time', default=0)
args = argparse.parse_args()

assert args.timeout > 0, "timeout should be > 0."
assert args.frequency > 0, "Sampling frequency should be >0."

comPort = args.port
logFile = args.file
timeout = args.timeout

#open serial communication with the microcontroler
ser   = serial.Serial(comPort,args.baudrate, timeout=args.timeout)
#when opening serial communication, some microcontroler reset and sends some data on the serial port
time.sleep(2)    #wait 2 seconds for this data 
ser.flushInput() #flush data we don't need 
#open the file
f = open(logFile,'w')
print("Reading from: "+comPort)
print("Writing to  : "+logFile)
#set the sampling frequency
cmd = 'f'+str(args.frequency)
ser.write(bytes(cmd,'ascii'))
freq = float(ser.readline())
print("Set the sampling frequency to "+str(freq)+"Hz")
#compute the number of samples we should have for the time specified !
nbsamples = int(freq * args.length)
print("The number of samples for a length of "+str(args.length)+"seconds is "+str(nbsamples))
print("------------------------")
print("Begin grabbing samples in 5 seconds - press CTRL+C in order to stop it abruptly !");
time.sleep(5)
tstart = time.time()    #start timestamp
ts = tstart #last sample timestamp
if args.method == 0:  # method grab samples
    ser.write(b'G-1') #no need to specify the number of samples in this case
                      #but a condition on the length have to be specified
    while time.time()-ts<timeout and time.time()-tstart<args.length:    
        try:                
            data = ser.readline().decode()        
            if len(data) != 0:
                ts=time.time() #last sample timestamp
                data = str(ts)+" "+data #add a timestamp to the data 
                f.write(data)
                sys.stdout.write(data) # print data to the terminal
                sys.stdout.flush()                    
        except:
            print('------------------------')
            print('Stopped abruptly after',time.time()-tstart,'seconds')            
            f.close() #close file
            break
    ser.write(b'G0') # grabbing data infinitively should always be stopped, why ?     
elif args.method == 1: # method Samples and not samples, so we can stop it
    cmd = 'S'+str(nbsamples)
    ser.write(bytes(cmd,'ascii')) 
    # after receiving nbsamples it should stop automatically with the specified timeout (10sec by default)
    while time.time()-ts<timeout: 
    #while time.time()-ts<timeout and time.time()-tstart<args.length:    #uncomment this if you want to be sure to stop before specified length
        try:                
            data = ser.readline().decode() #decode convert to string
            if len(data) != 0:
                ts=time.time() #last sample timestamp
                data = str(ts)+" "+data #add a timestamp to the data 
                f.write(data)
                sys.stdout.write(data) # print data to the terminal
                sys.stdout.flush()                    
        except:
            print('------------------------')
            print('Stopped abruptly after',time.time()-tstart,'seconds')
            ser.write(b'S0')  # stop sample to avoid sending data             
            break
elif args.method == 2: # method where we ask every 1/freq seconds, less precise than previsous method but control by the computer/script
    while time.time()-ts<timeout and time.time()-tstart<args.length:    
        try:              
            ser.write(b'A') # ask all data 
            data = ser.readline().decode() #decode convert to string
            if len(data) != 0:
                ts=time.time() #last sample timestamp
                data = str(ts)+" "+data #add a timestamp to the data 
                f.write(data)
                sys.stdout.write(data) # print data to the terminal
                sys.stdout.flush()                    
                time.sleep(1/freq) #wait 1/freq seconds before asking next samples
        except:
            print('------------------------')
            print('Stopped abruptly after',time.time()-tstart,'seconds')                        
            break
elif args.method == 3: # method similar to the previous one but take into account the accumulated time 
    nbacq = 0  # number of acquisition from the start
    while time.time()-tstart<args.length:
        try:              
            if time.time() - tstart >= nbacq/freq:
                ser.write(b'A')
                data = ser.readline().decode() #decode convert to string
                if len(data) != 0:
                    ts=time.time() #last sample timestamp
                    data = str(ts)+" "+data #add a timestamp to the data 
                    f.write(data)
                    sys.stdout.write(data) # print data to the terminal
                    sys.stdout.flush()
                    nbacq = nbacq+1            
        except:
            print('------------------------')
            print('Stopped abruptly after',time.time()-tstart,'seconds')                        
            break
else:
    print('No method',args.method,'implemented, specify 0 to 3 !')

print('Finish after',time.time()-tstart,'seconds')
f.close()


