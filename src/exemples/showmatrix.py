#!/usr/bin/env python
import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from argparse import ArgumentParser

#arguments parser
argparse = ArgumentParser()
argparse.add_argument('-f','--file', type=str, help='Input filename', default="matrix.txt")
argparse.add_argument('-o','--output', type=str, help='Output image filename with extension for the plot, e.g. matrix.png', default="")
argparse.add_argument('-p','--palet', type=str,help='Palet used to display matrix, see https://matplotlib.org/stable/tutorials/colors/colormaps.html', default='jet')
args = argparse.parse_args()

#load matrix from file
matrix = np.loadtxt(args.file,delimiter=" ") 

fig, ax = plt.subplots() #prepare a plot with image and axes
#palet https://matplotlib.org/stable/tutorials/colors/colormaps.html
im = ax.imshow(matrix,cmap=args.palet)
#for colorbar
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.2)
plt.colorbar(im,cax=cax)
if len(args.output):
    plt.savefig(args.output)
plt.show()