# Template du projet BA2 Biomed 2023-2024 (ULB Polytech)

Il s'agit d'un template pour créer votre répositoire dans le cadre du [projet BA2 Biomed 2023-2024](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2023-2024/enonce). Il suffit de faire un "_fork_" de ce projet vers un autre projet de votre groupe _BIOMEDx_ dans gitlab.

![forker](www/docs/img/fork.png)

Un répositoire sert principalement au stockage de fichiers _textes_, **le stockage de fichiers _binaires_ (vidéos, grosses images, etc.) devra donc être fortement limité**.

En pratique, la structure proposée, sous forme de dossiers, est la suivante :
- `arduino` contient les codes de votre microcontrôleur ainsi que les librairies : il faudra changer, dans les préférences de l'Arduino IDE, l'emplacement du carnet de croquis par ce dossier.
- `documentation` contient l'ensemble de votre documentation interne (vos recherches, vos références (liens), etc.).
- `gestion` contient l'ensemble des vos fichiers relatifs à la gestion de projet : "_Status Reports_" en Markdown, "risk and issues logs" en xlsx et votre fichier _GanttProject_ mis à jour régulièrement. 
- `src` contient vos codes python pour le pilotage du dispositif, l'enregistrement et le traitement des données et l'affichage des images thermiques.
- `www` contient la [documentation mise à disposition du public](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2023-2024/ircam), typiquement la méthodologie et un résumé de vos tests avec images et vidéos externes. Il utilise le framework [MkDocs](http://www.mkdocs.org/) pour mettre en ligne le contenu.

Vous trouverez en général dans chacun de ces dossiers un _README_ qui décrit plus spécifiquement son contenu.

**N'hésitez pas à modifier ce template par rapport à vos besoins !**

## Historique

- **Version 0.5 - 2023-08-18** : ajout d'exemples de communications séries et mises-à-jours de plusieurs contenus textuels.
- **Version 0.1 - 2023-08-14** : structure de base du template du projet BA2 2023-2024 

