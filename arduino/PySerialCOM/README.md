# PySerialCOM : communication série entre un microcontrôleur et un PC

[`PySerialCOM.ino`](./PySerialCOM.ino) est un exemple de code arduino permettant d'établir une communication série de base entre un microcontrôleur et un ordinateur.

Ce code utilise un protocole de communication ASCII qui consiste à envoyer un simple caractère par le port série et qui sera alors traité comme une commande par le microcontrôleur, éventuellement suivi d'un nombre entier ou flotant, par exemple pour configurer une variable, démarrer des acquisitions, etc.

La communication du côté PC peut s'effectuer grâce au moniteur série de l'[Arduino IDE](https://www.arduino.cc/en/software) ou un programme fait en python (cf. [exemples](../../src/exemples/)) ou tout autre langage de programmation.

En pratique, ce code arduino lit des données sur une entrée analogique (ADC) et 4 entrées numériques afin d'envoyer ces valeurs à un PC à une fréquence d'échantillonnage spécifiée (la plupart du temps avec un nombre fixe d'échantillons) ou sous forme de données matricielles (à vitesse maximale de lecture des entrées).

## Installation et configuration

Après avoir installé l'[Arduino IDE](https://www.arduino.cc/en/software) et vous être un peu familiarisé avec ce dernier grâce à des tutoriels sur internet, par exemple [celui-ci](https://www.youtube.com/watch?v=ikwBLy8RLMw), procédez aux étapes suivantes :

1. Dans vos préférences, faites pointer votre "_sketchbook_" vers le dossier arduino de ce répositoire qui a été préalablement cloné sur votre PC (cf. [cette vidéo](https://www.youtube.com/watch?v=xM7AK7R51zQ)).
2. Ouvrez ensuite le projet [`PySerialCOM.ino`](./PySerialCOM.ino) avec l'Arduino IDE.
3. Connectez un microcontrôleur compatible Arduino et configurez l'Arduino IDE avec les paramètres corrects de la carte du menu `Tools` c.à.d. le `Board` avec son port série qui s'est normalement ajouté dans le menu `Tools->Port` au moment du branchement.
4. Adaptez le début du fichier [`PySerialCOM.ino`](./PySerialCOM.ino) au microcontrôleur en vous aidant d'un schéma présentant son _pinout_ (i.e. ses [_GPIO_](https://fr.wikipedia.org/wiki/General_Purpose_Input/Output)) :
	- la définition `ANALOGPIN` avec un numéro de _GPIO_ disposant d'une entrée à un [ADC (Convertisseur analogique numérique)](https://fr.wikipedia.org/wiki/Convertisseur_analogique-num%C3%A9rique).
	- les valeurs du vecteur `const int din[]` avec 4 _GPIOs_ pouvant être configurés comme entrées numériques. Notez que vous pouvez augmenter ou diminuer le nombre de _GPIO_ utilisé comme entrées numériques mais n'oubliez pas dans ce cas d'adapter la valeur `const int nbdin` avec le nombre exact défini.
	- la définition `LEDPIN` avec le numéro de la patte sur laquelle une led est branchée sur la _BOARD_ de votre microcontrôleur. S'il n'y pas de led disponible, vous pouvez utiliser un _GPIO_ pouvant être configuré en sortie digitale.
5. Sur ces 5 _GPIO_, vous pouvez éventuellement connecter certains signaux, par exemple, un simple fil sur la broche analogique pour capter du bruit et quelques interrupteurs en [_pull up_ ou _pull down_](https://www.electrosoftcloud.com/en/arduino-pull-up-pull-down-resistors/) sur les broches numériques pour modifier leur état (0 ou 1) pendant les acquisitions.
6. Compilez et téléchargez le sketch sur votre microcontrôleur.
7. Ouvrez le moniteur série et sélectionnez la vitesse série par défaut 115200 (si vous ne l'avez pas modifiée avant de télécharger le code avec la définition `SERIAL_SPEED`).
8. Testez quelques commandes ci-dessous dans le moniteur série.


## Liste des commandes

| Commande | Variable       | Exemple | Description/réponse sur port série         
| ------   | ------         | ------  |------                                     |
| a        |                | a		  | Valeur de l'entrée analogique       |
| d        |[0,1 ndin[ int  | d1      | Entrée digitale spécifiée par la _Variable_ (valeurs 0 ou 1) en une ligne séparée par des espaces   |
| D        |                | D       | Toutes les entrées digitales (`ndin` valeurs) en une ligne séparée par des espaces |
| A        |		        | A		  | 1 échantillon i.e. entrée analogique et les entrées digitales (`ndin`+1 valeurs) |
| p        |[4 3600000] int | p100    | Période pour les acquisitions en ms            |
| f        |[0.00028 250]   | f10     | Fréquence pour les acquisitions en Hz (inverse de la période en sec) |
| s        | >=0 (int)      | s100    | _Variable_ échantillons inarrêtables (un échantillon par ligne)   |
| S        | >=0 (int)      | S1000   | _Variable_ échantillons arrêtables avec la comande S0  |
| G        | int            | G-1     | _Variable_ échantillons si positif sinon un nombre indifini d'échantillons arrêtables avec G0 |
| w        | >0             | w100    | Nombre de colonnes (largeur) de la matrice |
| h        | >0             | h90     | Nombre de rangées (hauteur) de la matrice  |
| c        | >0             | c0.2    | Configure le paramètre avec _Variable_ (facteur de conversion de la matrice)  |
| m        |                | m       | Matrice de la valeur de l'entrée analogique (multipliée par c) envoyée en h rangées et w colonnes séparées par des espaces |
| M        |                | M       | w x h échantillons avec indice de ligne et colonne en premier |
| l        |                | l       | Allumer/éteindre la led alternativement |


N'hésitez pas à jeter un coup d'œil à la fonction `void parseCMD()` pour plus d'informations sur les commandes et surtout leur implémentation !


## Exemple de suite de commandes

1. Réglez le facteur de conversion à 0.2 avec la commande `c0.2`
2. Réglez la largeur de la matrice à 110 avec la commande `w110`
3. Réglez la hauteur de la matrice à 90 avec la commande `h90`
4. Obtenez une valeur de la broche analogique avec la commande `a`
5. Obtenez la valeur analogique et toutes les valeurs/états numériques (o ou 1) avec la commande `A`
6. Réglez la fréquence d'échantillonnage à 1Hz avec la commande `f1`
7. Obtenez 200 valeurs à 1Hz avec la commande `S200`
8. Obtenez une matrice 110x90 avec la commande `m` 
8. etc.
